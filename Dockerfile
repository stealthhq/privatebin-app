FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

ARG RELEASE=1.3.3

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

COPY start.sh /app/pkg/

RUN \
# Install PrivateBin
    curl -Ls https://github.com/PrivateBin/PrivateBin/archive/${RELEASE}.tar.gz | tar -xzf - --strip 1 -C /app/code \
    && mv .htaccess.disabled .htaccess \
    && rm *.md \
    && mv /app/code/tpl /app/code/tpl.orig \
    && mv /app/code/img /app/code/img.orig \
    && mv /app/code/css /app/code/css.orig \
    && ln -s /app/data/conf.php /app/code/cfg/conf.php \
    && ln -s /app/data/templates /app/code/tpl \
    && ln -s /app/data/images /app/code/img \
    && ln -s /app/data/style /app/code/css \
    && ln -s /app/data/data /app/code/data \
    && chown -R www-data.www-data /app/code


# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
COPY apache/privatebin.conf /etc/apache2/sites-enabled/privatebin.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# configure mod_php
RUN a2enmod php7.2
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

CMD [ "/app/pkg/start.sh" ]
