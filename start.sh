#!/bin/bash

set -eu

mkdir -p /app/data/data
cp -R /app/code/tpl.orig /app/data/templates
cp -R /app/code/img.orig /app/data/images
cp -R /app/code/css.orig /app/data/style

echo "==> Creating config"
if [[ ! -f /app/data/conf.php ]]; then
    cp /app/code/cfg/conf.sample.php /app/data/conf.php

    # Enable file uploading
    sed -i "s#fileupload = false#fileupload = true#" /app/data/conf.php
fi

echo "==> Changing permissions"
chown -R www-data:www-data /app/data/

echo "==> Staring PrivateBin"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
