This app packages PrivateBin <upstream>1.3.3</upstream>

A minimalist, open source online pastebin where the server has zero knowledge of pasted data. Data is encrypted/decrypted in the browser using 256 bits AES. [https://privatebin.info/](https://privatebin.info/)

### What PrivateBin provides

* As a server administrator you don't have to worry if your users post content that is considered illegal in your country. You have no knowledge of any of the pastes content. If requested or enforced, you can delete any paste from your system.

* Pastebin-like system to store text documents, code samples, etc.

* Encryption of data sent to server.

* Possibility to set a password which is required to read the paste. It further protects a paste and prevents people stumbling upon your paste's link from being able to read it without the password.

