#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent');

var By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder;

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';

    var server, browser = new Builder().forBrowser('chrome').build();
    var app;

    before(function () {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();
    });

    after(function () {
        browser.quit();
    });

    const MESSAGE2 = 'Upload/Download Text File secret message';
    let pasteUrl;

    function checkUpload(done) {
        const testFilePath = `${path.resolve(__dirname, '.')}/test.txt`;

        browser.get('https://' + app.fqdn).then(function() {
            return browser.findElement(By.id('message')).sendKeys(MESSAGE2);
        }).then(function() {
            return browser.findElement(By.id('file')).sendKeys(testFilePath);
        }).then(function() {
            return browser.findElement(By.id('attach')).click();
        }).then(function() {
            return browser.findElement(By.id('sendbutton')).click();
        }).then(function() {
            return browser.sleep(5000);
        }).then(function() {
            return browser.findElement(By.id('pasteurl')).getText();
        }).then(function(url) {
            pasteUrl = url;
            console.log(`pasteUrl is ${pasteUrl}`);
            // pasteUrl is our unique secret URL
            return ;
        }).then(function() {
            done();
        });
    }

    function checkDownload(done) {
        browser.get('about:blank').then(function () { // chrome doesn't reload if url is same
            return browser.sleep(2000);
        }).then(function () {
            browser.get(pasteUrl);
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            return browser.findElement(By.id('prettyprint')).getText();
        }).then(function(text) {
            if (text !== MESSAGE2) return Promise.reject(new Error(`Message mismatch: Actual: ${text} Expected: ${MESSAGE2}`));
        }).then(function () {
            return browser.findElement(By.id('attachment')).getText();
        }).then(function(text) {
            if (!text.includes('Download attachment')) return Promise.reject(new Error('No attachment'));
        }).then(function() {
            return browser.findElement(By.xpath('//div[@id="attachment"]/a')).getAttribute('href');
        }).then(function(href) {
            if (!href.includes(app.fqdn)) return Promise.reject(new Error('href does not include app fqdn'));

            console.log(href);
            return browser.get(href); // Let's view our text file in the browser
        }).then(function() {
            return browser.sleep(2000);
        }).then(function() {
            return browser.findElement(By.tagName('body')).getText();
        }).then(function(bodyContents) {
            if (bodyContents !== 'Test txt file upload') return Promise.reject(new Error('File contents does not match'));

            done();
        }).catch(done);
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can upload file', checkUpload);
    it('can download flie', checkDownload);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can download flie', checkDownload);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can download flie', checkDownload);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install previous version from appstore', function () {
        execSync(`cloudron install --appstore-id info.privatebin.cloudronapp --location ${LOCATION}`);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can upload file', checkUpload);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can download flie', checkDownload);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
